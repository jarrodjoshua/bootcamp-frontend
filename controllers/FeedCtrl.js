'use strict';

angular.module('myApp.mainFeed', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/mainFeed', {
    templateUrl: '/mainFeed.html',
    controller: 'FeedCtrl'
  });
}])

.controller('FeedCtrl', ['$scope', '$http', function($scope, $http) {

      //This will hide the DIV by default.
      $scope.IsHidden = true;
      $scope.ShowHide = function () {
          //If DIV is hidden it will be visible and vice versa.
          $scope.IsHidden = $scope.IsHidden ? false : true;
      }
    //   $scope.records = [
    //    {
    //         "Name" : "Alfreds Futterkiste",
    //         "Rank" : "Cub",
    //         "Place" : "Paris, France",
    //         "Review" : "Loved Paris, really beautiful city.",
    //         "Image" : "https://media-cdn.tripadvisor.com/media/photo-s/12/f8/68/3d/big-bus-paris-hop-on.jpg"
    //
    //     },{
    //         "Name" : "Berglunds snabbköp",
    //         "Rank" : "Cub",
    //         "Place" : "Berlin, germany",
    //         "Review" : "Great food and beer.",
    //         "Image" : "https://www.visitberlin.de/system/files/styles/visitberlin_bleed_header_visitberlin_mobile_1x/private/image/iStock_000074120341_Double_DL_PPT_0.jpg?h=a66ba266&itok=2YXS5_33"
    //     },{
    //         "Name" : "Centro comercial Moctezuma",
    //         "Rank" : "Explorer",
    //         "Place" : "Sau Paulo, Brazil",
    //         "Review" : "A bit dirty but the people were friendly.",
    //         "Image" : "https://thumbnails.trvl-media.com/hgLs8e-KyF5yHJBR4o1pCbw9DNU=/768x432/images.trvl-media.com/media/content/shared/images/travelguides/destination/180023/Sao-Paulo-Cathedral-64700.jpg"
    //     },{
    //         "Name" : "Ernst Handel",
    //         "Rank" : "Scout",
    //         "Place" : "Sydney, Australia",
    //         "Review" : "Expensive but worth the visit.",
    //         "Image" : "https://www.australia.com/content/australia/en/places/sydney-and-surrounds/getting-around-sydney/jcr:content/image.adapt.414.medium.jpg"
    //     }
    // ]

    $http.get("http://localhost:8080/maven-app/webapi/user/read?username=kjanjuha")
      .then(function (response) {$scope.records = response.data;});

  }]);

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.mainFeed',
    'myApp.meetUp',
])
    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.otherwise({redirectTo: '/mainFeed'});
}]);

'use strict';

angular.module('myApp.meetUp', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/meetUp', {
    templateUrl: '/meetUp.html',
    controller: 'meetUpCtrl'
  });
}])

.controller('meetUpCtrl', ['$scope', function($scope) {
  //This will hide the DIV by default.
  $scope.IsHidden = true;
  $scope.ShowHide = function () {
      //If DIV is hidden it will be visible and vice versa.
      $scope.IsHidden = $scope.IsHidden ? false : true;
  }
  $scope.records = [
"Ellen Leahy",
"Jarrod Joshua",
"Karan Css",
"Jon Mash",
"Tinashe Android",
]

}]);

$( document ).ready(function() {
	$('#hotelresults').hide();
	$('#errorModal').hide();
	//Run flight info first
	findHotelInfo("2018-10-07", "2018-10-09");
	applyClickListeners();
	 console.log(API_ADDRESS_CONSTANT)
});


function findHotelInfo(dateFrom, dateTo){

		$.ajax(API_ADDRESS_CONSTANT + 'hotel?latitude=39.6953&longitude=3.0176&radius=42&checkIn=' + dateFrom + "&checkOut=" + dateTo, 
		{
			//Type of response data
		    dataType: 'json',
		    success: function (data) { 

			     console.log(data);
			     appendInfo(data);
		    }
		});
}


function appendInfo(arrayOfHotels){

	var array = arrayOfHotels;
	var htmlToAppend ="";

	for (i = 0; i < 10; i++) {
			var marketingText = array[i].marketingText;

			if (marketingText == null){
				marketingText = "No description available at this time"
			} else {
				marketingText = marketingText.substring(0, 100) + "...";
			}

			htmlToAppend += hotelContainer(array[i].hotelName, array[i].city, marketingText, array[i].bedType, array[i].totalPrice, i, array[i]);

		}

	$('#hotelresults').append(htmlToAppend);

	fadeOutLoader();

}

function hotelContainer(hotelName, city, marketingText, bedType, totalPrice, index, array){

	var hotelHTML = 	

	'<div class="singleHotelContainer" style="padding-bottom: 80px;">' +

		'<div style="display: table-cell; vertical-align: middle;">' +
			'<img src="https://content.skyscnr.com/1017dd4cef7a773021e1b335b3410d30/GettyImages-487828389.jpg" style=" border-radius: 6px; max-width: 250px; max-height: 305px;">' +
		'</div>' +


		'<div style="display: table-cell; vertical-align: middle; text-align: left; padding-left: 30px; ">' +
			'<p style="margin: 0; font-weight: 400;">' + hotelName + '</p>' +
			'<p style="margin: 0; ">' + city + '</p>' +

			'<div style ="text-align: left; padding-top: 5px;">' +
					'<div style="display: flex;">' +
						'<img src="./images/star-yellow.png" height="15" width="15">' +
						'<img src="./images/star-yellow.png" height="15" width="15">' +
						'<img src="./images/star-yellow.png" height="15" width="15">' +
					'</div>' +
			'</div>' +
			'<p style="margin: 0; font-size: 13px; width: 250px; padding-top: 10px;">' + marketingText + '</p>' +
		'</div>' +

		'<div style="display: table-cell; vertical-align: middle; padding-left:20px; width: 200px">' +
			'<p style="margin:0">Priced at</p>' + 
			'<p style="color: #248a37;">£' + totalPrice + '</p>' +
			'<p id="flightJSON" style="font-size: 0px;">' + JSON.stringify(array) + '</p>' +
			'<a class="c-btn c-btn--secondary" onclick="selectedHotel(this,' + index + ')">Select</a>' +
		'</div>' +

	'</div>';

	return hotelHTML;
}


function fadeOutLoader(){

	 $('#loader').delay(5000).show().fadeOut('slow', function(){
        $('#hotelresults').delay(10000).show().fadeIn('slow');
    });
}

function applyClickListeners(){

//checkIfHotelSelected
$( "#nextBtn" ).click(function() {
		
	if (localStorage.getItem("hasSelectedHotel") == null) {
		$('#errorModal').show();
	} else {
		window.location.href = "./TravelPlusPlus-Confirm.html";
	}
});


//Close modal
$( "#closeModal" ).click(function() {
	$('#errorModal').hide();
});

}


var selectedHotel = function(btn, value) {
  var arrOfSelectableBtns = $('#hotelresults').find('.c-btn.c-btn--secondary');

    for (var i = 0; i < arrOfSelectableBtns.length; i++) {
    	arrOfSelectableBtns[i].innerHTML = "Select"
    	$(arrOfSelectableBtns[i]).css( "color", "#0073c5" );
    	$(arrOfSelectableBtns[i]).css( "background-color", "transparent" );
    }

    var hotelJSON = $(document.getElementsByClassName("c-btn c-btn--secondary")[value]).prev()[0].innerHTML;

    btn.innerHTML = "Selected";
    $(btn).css( "color", "#fff" );
    $(btn).css( "background-color", "#0073c5" );

    setLocalStorage(hotelJSON)
  
};



function setLocalStorage(values){
	var obj = JSON.parse(values);
	Object.keys(obj).forEach(function(k){
		localStorage.setItem(k, obj[k]);
    	console.log(k + ' - ' + obj[k]);
	});
	localStorage.setItem("hasSelectedHotel", true);
}

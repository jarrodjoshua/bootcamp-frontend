$( document ).ready(function() {
	$('#errorModal').hide();
	$('#flightresults').hide();
	//Run flight info first
	findFlightInfo();
	applyClickListeners();
});


function findFlightInfo(){

		$.ajax(API_ADDRESS_CONSTANT + 'flight?originAirport=LHR&destinationAirport=LAX&departureDate=2018-10-07&returnDate=2018-10-09&noOfAdults=2&noOfChildren=2', 
		{
			//Type of response data
		    dataType: 'json',
		    success: function (data) { 
			     console.log(data);
			     appendInfo(data);
		    }
		});
}


function appendInfo(arrayOfFlights){

	var array = arrayOfFlights;
	var htmlToAppend ="";
	var routeHTMLToAppend ="";

	for (i = 0; i < array.length; i++) {
		var outboundArray = array[i].flightOutboundSegmentList;
		var departureAirport = array[i].fromAirport;
		var arrivalAirport = array[i].toAirport;


		//If 1 is less than 3 then add the dividers
		//3 is the length of the outbound list
		//Therefore start loop at 1
		for (z = 0; z < outboundArray.length; z++){

			//3 - 1 = 2
			var numTimesToRun = outboundArray.length - 1;

			routeHTMLToAppend += makeHTMLRoute(
							outboundArray[z].departureAirportName,
							outboundArray[z].arrivalAirportName,

							outboundArray[z].flightNumber,
							outboundArray[z].destinationTerminal,
							outboundArray[z].aircraft,
							outboundArray[z].departureDateTime,
							outboundArray[z].arrivalDateTime
							);

			//If
			if (z < numTimesToRun){
				routeHTMLToAppend += makeHTMLDividers();
			}

		}

	 	htmlToAppend += 	
	 		    			flightBriefContainer(
	 		    									array[i].outboundFlightTime,
	 		    									array[i].outboundFlightTimeDuration,
	 		    									array[i].returnFlightTime,
	 		    									array[i].returnFlightTimeDuration,
	 		    									array[i].outboundAirlineName,
	 		    									array[i].outboundFromToAirport,
	 		    									array[i].returnAirlineName,
													array[i].returnFromToAirport,
													array[i].fareTotalPrice,
													array[i].outboundSeatsRemaining
												) +
							'<div class="acc_container">' +
							'<div class="block">' + routeHTMLToAppend + '</div>' +

							'<div style="padding: 0px 40px 40px 50px; text-align: right">' +
								'<p id="flightJSON" style="font-size: 0px;">' + JSON.stringify(array[i]) + '</p>' +
								'<a class="c-btn c-btn--secondary" value="' +  i  + '" onclick="selectedFlight(this,' + i + ')">Select</a>' +
							'</div>' +

							'</div>'

		//Clear the route to append at the end of each WHOLE loop so that we dont get a massive route.
		routeHTMLToAppend = "";

	}


	$("#accordion").append(htmlToAppend);

    //Set default open/close settings
    var divs=$('#accordion>div').hide(); //Hide/close all containers	

   
    var h2s=$('#accordion>h2').click(function () {

        h2s.not(this).removeClass('active')
        $(this).toggleClass('active')
        divs.not($(this).next()).slideUp()
        $(this).next().slideToggle()
        return false; //Prevent the browser jump to the link anchor
	
    });

    fadeOutLoader();
}



function fadeOutLoader(){
	//$('#loader').delay(5000).show().fadeOut('slow');
	//Pass data just for appending after fade out
//	$('#flightresults').delay(10000).show().fadeIn('slow');

	 $('#loader').delay(5000).show().fadeOut('slow', function(){
        $('#flightresults').delay(10000).show().fadeIn('slow');
    });
}


function makeHTMLRoute(startDest, endDest, flightNo, terminalNo, aircraft, departureTime, arrivalTime){


var htmlPole = '<div class="timeline" style="padding: 20px 20px 20px 100px; display: table; vertical-align: middle; position: relative;">' +

			'<div style="font-size: 16px; margin-left: 130px; text-align: left; position: absolute; left: 0; top: 12px; width: 300px; font-weight: 400;">' + startDest + '</div>' +

				'<div style="position: relative; display: inline-block; float: left;">' +
					'<div style="position: relative;">' +
						'<div class="rectangle">' +
						  '<div class="topcircle"></div>' +
						  '<div class="bottomcircle"></div>' +
						'</div>' +
					'</div>' +
				'</div>' +

				'<div style="text-align: left; display: table-cell; padding-left: 40px; vertical-align: middle; font-size: 14px">' +
					'<div><span style="margin:0; width: 80px; display: inline-block;">Flight:</span>    <span>' + flightNo + '</span></div>' +
					'<div><span style="margin:0; width: 80px; display: inline-block;">Terminal:</span>  <span>' + terminalNo + '</span></div>' +
					'<div><span style="margin:0; width: 80px; display: inline-block;">Aircraft:</span>  <span>' + aircraft + '</span></div>' +
                	'<div><span style="margin:0; width: 80px; display: inline-block;">Departure:</span> <span>' + departureTime + '</span></div>' +
                	'<div><span style="margin:0; width: 80px; display: inline-block;">Arrival: </span>  <span>' + arrivalTime + '</span></div>' +
				'</div>' +

			'<div style="font-size: 16px; margin-left: 130px; text-align: left; position: absolute; left: 0; bottom: 0; width: 300px; font-weight: 400;">' + endDest + '</div>' +

		'</div>';

return htmlPole;

}


function makeHTMLDividers(){
	var dividers = 	'<div class="dividercircle"></div>' +
					'<div class="dividercircle"></div>' +
					'<div class="dividercircle"></div>' ;

	return dividers;
}

function flightBriefContainer(
	outboundFlightTime,
	outboundFlightTimeDuration,
	returnFlightTime,
	returnFlightTimeDuration,
	outboundAirlineName,
	outboundFromToAirport,
	returnAirlineName,
	returnFromToAirport,
	fareTotal,
	outboundSeatsRemaining
	){
	var flightBriefHTML = 

'<h2 class="acc_trigger">' +

	'<div style="display: table;">' +

		'<div style="display: table; padding-top: 20px">' +


			'<div style="display: table-cell; padding-left: 30px; padding-right: 30px; vertical-align: middle;">' +
				'<p style="font-size: 16px; margin: 0; font-weight: 300;">' + outboundFlightTime.substring(0, 5) + '</p>' +
				'<p style="color: #616161; font-size: 12px; margin: 0; font-weight: 300;">'+ outboundFromToAirport.substring(0,3) +'</p>' +
			'</div>' +

			'<div style="display: table-cell; position: relative; vertical-align: middle;">' +
				'<hr style="width: 200px; height: 1px; background-color: black">' +

				'<div class = "blackdot1">' +
				'</div>' +

				'<div class = "blackdot2">' +
				'</div>' +
			'</div>' +

			'<div style="display: table-cell; padding-left: 40px; padding-right: 30px; vertical-align: middle;">' +
				'<p style="font-size: 16px; margin: 0; font-weight: 300;">' + outboundFlightTime.substring(outboundFlightTime.length-5, outboundFlightTime.length) + '</p>' +
				'<p style="color: #616161; font-size: 12px; margin: 0; font-weight: 300;">'+ outboundFromToAirport.substring(outboundFromToAirport.length-3,outboundFromToAirport.length) +'</p>' +
			'</div>' +



			'<div style="display: table-cell; width: 200px; height: 100px; background-color: #214F74; color: white; border-radius: 6px; padding: 10px">' +
				'<div style="text-align: left;">' +
					'<p style="font-size: 16px; margin: 0">Economy</p>' +
					'<p style="font-size: 12px; margin: 0; width: 100px;">Return - 2 adults, 2 Children</p>' +
				'</div>' +
				'<div>' +
				'<p style="font-size: 16px; margin: 0">£' + fareTotal + '</p>' +
				'</div>' +
				
			'</div>' +


			'<div style="width: 110px;height: 100px;background-color: #b73e3e;color: white;border-radius: 6px; padding: 10px; margin-left: 10px;">' +
				'<div style="text-align: left;">' +
					'<p style="font-size: 16px; margin: 0">Seats Remaining</p>' +
				'</div>' +
				'<div>' +
				'<p style="font-size: 16px; margin: 0; padding-top: 5px;">' + outboundSeatsRemaining + '</p>' +
				'</div>' +
				
			'</div>' +


		'</div>' +

	'</div>' +

'</h2>';

return flightBriefHTML;


}



function applyClickListeners(){

//checkIfFlightSelected
$( "#nextBtn" ).click(function() {
		
	if (localStorage.getItem("hasSelectedFlight") == null) {
		$('#errorModal').show();
	} else {
		window.location.href = "./TravelPlusPlus-BookHotel.html";
	}
});


//Close modal
$( "#closeModal" ).click(function() {
	$('#errorModal').hide();
});

}


var selectedFlight = function(btn, value) {
  //Clear local storage
  localStorage.clear();
  
  var arrOfSelectableBtns = $('#flightresults').find(".c-btn.c-btn--secondary");

    for (var i = 0; i < arrOfSelectableBtns.length; i++) {
    	arrOfSelectableBtns[i].innerHTML = "Select"
    	$(arrOfSelectableBtns[i]).css( "color", "#0073c5" );
    	$(arrOfSelectableBtns[i]).css( "background-color", "transparent" );
    }

    var flightJSON = $(document.getElementsByClassName("c-btn c-btn--secondary")[value]).prev()[0].innerHTML;

    btn.innerHTML = "Selected";
    $(btn).css( "color", "#fff" );
    $(btn).css( "background-color", "#0073c5" );

    setLocalStorage(flightJSON)
};



function setLocalStorage(values){
	var obj = JSON.parse(values);
	Object.keys(obj).forEach(function(k){
		localStorage.setItem(k, obj[k]);
    	console.log(k + ' - ' + obj[k]);
	});
	localStorage.setItem("hasSelectedFlight", true);
}



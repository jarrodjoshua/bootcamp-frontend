
$( document ).ready(function() {
	getCurrentDateTime();

});

function getCurrentDateTime(){
var d = new Date();
formatAMPM(d);

}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes;

document.getElementsByClassName("container-right")[0].childNodes[3].innerHTML = '<p style="color: #5B5B5B">' + strTime + '<span style="font-size: 12px"> ' + ampm +  '</span></p>';

getFormattedDate(date)

}


function getFormattedDate(today) 
{
    var week = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    var day  = week[today.getDay()];
    var dd   = today.getDate();
    var mm   = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var hour = today.getHours();
    var minu = today.getMinutes();

    const monthNames = ["January", "February", "March", "April", "May", "June",
	  "July", "August", "September", "October", "November", "December"
	];

    if(dd<10)  { dd='0'+dd } 
    if(mm<10)  { mm='0'+mm } 
    if(minu<10){ minu='0'+minu } 

	document.getElementsByClassName("container-right")[0].childNodes[5].innerHTML = day + ' ' + monthNames[today.getMonth()].substring(0, 3) + ' ' + dd;
}

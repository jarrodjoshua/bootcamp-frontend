from tomcat
maintainer jarroddocker

copy ./ /usr/local/tomcat/webapps/

expose 8080

CMD ["catalina.sh", "run"]
